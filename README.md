## Compilar y guardar cambios de config en barebox

Esto es mas o menos lo mismo que está en scripts/builds_barebox y scripts/rebuild_barebox

    cd /path/to/repo
    source scripts/environmen_vars.sh

    cd barebox 

    make mostro_mlo_defconfig
    make menuconfig # opcional
    make savedefconfig # opcional
    cp defconfig arch/arm/configs/mostro_mlo_defconfig #opcional
    make
    cp images/barebox-am33xx-beaglebone-mlo.img ../bin/MLO

    make clean
    make mostro_defconfig
    make menuconfig # opcional
    make savedefconfig # opcional
    cp defconfig arch/arm/configs/mostro_defconfig #opcional
    make
    cp images/barebox-am33xx-beaglebone.img ../bin/barebox.bin

## Instrucciones.

La secuencia de inicialización del sistema es así, a grandes rasgos:

En hardware busca una partición /root con flag "bootable" en la tarjeta sd (mas prioridad) o en la memoria interna mmc (si no hay sd). En esta partición /boot hay un binario "MLO" que es el first stage bootloader, que se compila con `make mostro_mlo_defconfig` como dice en este readme. Lo único que el MLO hace al correr es cargar el second stage bootloader, que es el binario barebox.bin, que se compila con `make mostro_defconfig` como dice arriba.

Para interactuar con la consola del second stage bootloader, se conecta la beagle por puerto serial/USB (cable ftdi) al compu y en el compu se abre una terminal serial: `sudo screen /dev/ttyUSB0 115200 scrollback 100000`. Luego dejamos espichada la tecla 0 (número cero) en el teclado del compu y encendemos la beagle. Esto evita que el second stage bootloader termine de bootear y muestra una terminal para interactuar con ella. 

En esa terminal podemos ver que hay unos archivos en /env/boot/sd y /env/init que son los que se editan para decirle al bootloader que hacer antes de bootear (prender leds, por ej) y como bootear el linux kernel. Algo asi:

    edit /env/init/leds
    edit /env/boot/sd
    saveenv

El comando saveenv guarda un binario barebox.env en la parición /boot. Cuando el bootloader intenta bootear, lee ese archivo y bootea segun esa configuración.

Cuando no hay un archivo /boot/barebox.env, se corre el script "default" que está compilado dentro de barebox.bin. Este archivo se puede editar en modo interactivo del bootloader, pero para que quede en los repos y se agarre en las imagenes generadas desde el repo de linux-filesystem, se edita en este repo, en barebox/arch/arm/boards/beaglebone/defaultenv-beaglebone/boot/sd 

Pillese que en la terminal del bootloader se tiene acceso a esa carpeta /env que en realidad es el archivo binario barebox.env. Al hacer `saveenv`, los cambios que uno hizo en esa carpeta quedan quemados en /boot/barebox.env

Entonces, cuando se bootea la beagle en modo normal (no interactivo) lo que pasa es que se inicia el linux kernel y se corre el script /etc/init.mostro.sh. En ese script hay varios condicionales que cargan drivers, arranca el patch de pd, flashea la mmc si es necesario, hace firmware update, etc.

También, el bootloader setea opciones de init del kernel que se pueden leer desde el linux andando al leer el archivo /proc/cmdline

