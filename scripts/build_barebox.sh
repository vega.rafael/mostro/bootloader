#! /bin/bash

COMPILER_PATH=`pwd`/../linux-kernel/dl/gcc-arm-8.3-2019.03-x86_64-arm-linux-gnueabihf/bin
export ARCH=arm
export CROSS_COMPILE=${COMPILER_PATH}/arm-linux-gnueabihf-

${CC}gcc --version

mkdir -p bin
cd barebox
make clean
make mostro_mlo_defconfig 
make
cp -f images/barebox-am33xx-beaglebone-mlo.img ../bin/MLO

make clean
make mostro_defconfig 
make
cp -f images/barebox-am33xx-beaglebone.img ../bin/barebox.bin
make clean

