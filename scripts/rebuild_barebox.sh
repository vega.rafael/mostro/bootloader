#! /bin/bash

COMPILER_PATH=`pwd`/../linux-kernel/dl/gcc-arm-8.3-2019.03-x86_64-arm-linux-gnueabihf/bin
export ARCH=arm
export CROSS_COMPILE=${COMPILER_PATH}/arm-linux-gnueabihf-

mkdir -p bin
cd barebox
# make mostro_defconfig 
make
cp images/barebox-am33xx-beaglebone.img ../bin/barebox.bin

